#include<stdio.h>
int main()
{
int first, second, *p, *q, sum,diff,prod,rem;
float div;
printf("Enter two integers \n");
scanf("%d%d", &first, &second);
p = &first;
q = &second;
sum = *p + *q;
diff = *p - *q;
prod = *p * *q;
div =(float)*p / *q;
rem = *p % *q;
printf("Sum of the numbers = %d\n", sum);
printf("Difference of the numbers = %d\n", diff);
printf("Product of the numbers = %d\n", prod);
printf("Division of the numbers = %f\n", div);
printf("Remainder of the numbers when divided  = %d\n", rem);
return 0;
}