#include<stdio.h>
int main()
{
	int i,j,r,c,a[20][20];
	printf("Enter the number of rows and columns \n");
	scanf("%d%d",&r,&c);
	printf("Enter the elements ");
	for(i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
		{
			printf("Enter element [%d,%d] : ",i+1,j+1);
			scanf("%d",&a[i][j]);
		}
	}
	printf("\nMatrix is :\n");
	for(i=0;i<r;i++)
	{
		for(j=0;j<c;j++)
		{
			printf("\t %d",a[i][j]);
		}
		printf("\n");
	}

	return 0;
}