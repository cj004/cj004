#include<stdio.h>
float factorial(int);
int main()
{
    int i,N;
    float sum,fac;
    printf("Enter the value of N: ");
    scanf("%d",&N);
    sum=0;
    for(i=1;i<=N;i++)
    {
        fac=factorial(i);
        sum = sum + ( (i) / (fac) );
    }
    printf("Sum of the series is: %f\n",sum);
    return 0;
}
float factorial(int num)
{
    int i;
    float fact=1;
    for(i=1; i<=num; i++)
        fact= fact*i;
    return fact;
}