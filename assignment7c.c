#include<stdio.h>
#include<math.h>   
int main()
{
    int i,N;
    float sum;
    int count;
    printf("Enter total number of terms: ");
    scanf("%d",&N);
    sum=0;
    count=1;
    for(i=1;i<=N;i++)
    {
        sum = sum + ( (pow(count,2)) / (pow(count,3)) );
        count=count+2;
    }	
    printf("Sum of the series is: %f\n",sum);
    return 0;
}